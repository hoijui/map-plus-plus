# - Try to find the LAPACK library C version (lapacke)
# Once done, this will define:
#  LAPACKE_FOUND         - system has lapacke
#  LAPACKE_INCLUDE_DIR   - the lapacke include directory
#  LAPACKE_LIBRARIES     - Link these to use lapacke
#

INCLUDE(FindPackageHandleStandardArgs)

IF    (LAPACKE_INCLUDE_DIR)
	# Already in cache, be silent
	SET(LAPACKE_FIND_QUIETLY TRUE)
ENDIF ()

FIND_PATH(LAPACKE_INCLUDE_DIR
	NAMES
		lapacke.h
		lapack/lapacke.h
	PATHS
		$ENV{CPATH}
		/usr/include
		/usr/local/include
	NO_DEFAULT_PATH
	)
FIND_PATH(LAPACKE_INCLUDE_DIR NAMES lapacke.h lapack/lapacke.h)

FIND_LIBRARY(LAPACKE_LIBRARIES
	NAMES
		lapacke
	PATHS
		$ENV{LD_LIBRARY_PATH}
		$ENV{LIBRARY_PATH}
		/usr/lib64
		/usr/lib
		/usr/local/lib64
		/usr/local/lib
	NO_DEFAULT_PATH
	)
FIND_LIBRARY(LAPACKE_LIBRARIES NAMES lapacke)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(LAPACKE DEFAULT_MSG LAPACKE_INCLUDE_DIR LAPACKE_LIBRARIES)

# show the LAPACKE_INCLUDE_DIR and LAPACKE_LIBRARIES variables only in the advanced view
MARK_AS_ADVANCED(LAPACKE_INCLUDE_DIR LAPACKE_LIBRARIES)

